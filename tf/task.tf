resource "google_compute_instance" "default" {
    count = 3
    name = "${count.index + 1}"
    machine_type = "e2-small"
    zone = "us-central1-a"
    boot_disk {
        initialize_params {
            image = "debian-cloud/debian-9"
            size = "20"
        }
    }
    
    network_interface {
        network = "default"
    }
}
